from django.shortcuts import render, get_object_or_404
from todos.models import TodoList

# show_todo_list
def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list_list": list,
    }
    return render(request, "todos/list.html", context)

    # def recipe_list(request):
    # recipes = Recipe.objects.all()
    # context = {
    # "recipe_list": recipes
    # }
    # return render(request, "recipes/list.html", context)
